#!/bin/bash

# First parameter : keywords
# Second paramter : content to search the keywords for

if [[ $1 = "Inconnu" ]]; then
	echo 999
	exit
fi

score=0
size_of_keyword=`wc -w <<< "$1"`

for i in $1
do
	echo $2 | grep -F $i > /dev/null
	if [[ $? -eq 0 ]]; then
		let "score++"
	fi
done

let "result = (score * 1000) / (size_of_keyword) + size_of_keyword"

echo -n $result
# printf "%004d" $result