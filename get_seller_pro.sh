#!/bin/bash

# This script display, in standard outline,
# True if seller is a pro
# False if seller is not a professionnal
# Parameter : the html file of an add
# Or the http reference of the add itself

status=`./get.sh seller_status $1`
if [[ ${status} = "pro" ]]; then
	echo true
else
	echo false
fi