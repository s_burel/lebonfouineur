if [[ $# -eq 1 && $1 = "-g" ]]; then
echo "Generating html content."
./generate_html_form_brand.sh > html/brands_form.php
./generate_html_form_model.sh > html/models_form.php
./generate_html_form_car_type.sh > html/car_type_form.php
./generate_html_form_fuel.sh > html/fuel_form.php
./generate_html_car_type_buttons.sh > html/car_type_buttons.php
./generate_html_brand_buttons.sh > html/brands_buttons.php
fi
echo "Searching for security leaks..."
leaks=0
for i in html/* ; do 
	grep "<?php include 'security.php'; ?>" $i > /dev/null
	if [[ $? -ne 0 ]] ; then
		echo "File ${i} have a security leak"
		leaks=1
	fi
done
if [[ $leaks -ne 0 ]]; then
	echo "Errors"
	echo "Some file include security leaks : Script auto-terminate without copying files"
	exit 1
fi
echo "Copying html content to /var/www/html..."
cp -r html/* /var/www/html/
grep -v profiling html/adds.php > /var/www/html/adds.php
echo "Done."
exit 0
