#!/bin/bash

echo "<?php include 'security.php'; ?>"
echo "<select name='car_type' id='car_type_form' class='form-control'>"
echo "<option value='0'>Classe du véhicule</option>"

request="select car_type_id, car_type_name from model natural join car_type natural join modelstats where model_price_average_cleaned is not null group by car_type_id"
list=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2`


# echo "$list"


while [[ -n $list ]]; do
	buffer=`head -n 1 <<< "$list"`
	list=`tail -n+2 <<< "$list"`
	echo "<option value='`cut -f1 <<< "$buffer"`'>`cut -f2 <<< "$buffer"`</option>"
done





echo "</select>"

echo "<?php "
echo "\$buffer=0;"
echo "if (isset(\$_GET['car_type']) and \$_GET['car_type'] != 0)"
echo "	\$buffer = \$_GET['car_type'];"
echo "echo \"<script language='javascript'>document.getElementById('car_type_form').value='\".\$buffer.\"';"
echo "</script>\" ; ?>"
