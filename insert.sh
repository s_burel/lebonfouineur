#!/bin/bash
# echo "Inserting $1"

# Security to avoid inserting existing add

add_id=`./get.sh id $1`
add_exist=`echo "select count(1) as '' from adds where add_id = ${add_id}" | mysql --defaults-extra-file=mysql.cnf`

if [[ ${add_exist} -eq 1 ]]; then # Security to avoid adding existing add
	exit 0
fi



request=
test=
data_unknown=`echo "select status_id as '' from status where status_name \
like \"Inconnu\"" | mysql --defaults-extra-file=mysql.cnf`


# Gearbox
gearbox_id=`./get.sh gearbox_id $1`

# Fuel type
fuel_id=`./get.sh fuel_id $1`
if [[ $fuel_id = "NaN" ]]; then 
	fuel_id=-1
fi

# Seller
# Check if seller exist
seller_id=`./get.sh seller_id $1`
request="select count(1) as '' from seller where seller_id=${seller_id}"
test=`echo $request | mysql --defaults-extra-file=mysql.cnf`
if [[ ${test} -eq 0 ]]; then
	seller_name=`./get.sh seller_name $1`
	seller_nosalesman=`./get.sh no_salesmen $1`
	if [[ -z ${seller_nosalesman} ]]; then
		seller_nosalesman=true
	fi
	seller_phone=`./get.sh seller_phone $1`
	if [[ -z ${seller_phone} ]]; then
		seller_phone=0000000000
	fi
	seller_pro=`./get_seller_pro.sh $1`
	request="insert ignore  	into seller values(\
			${seller_id},\"${seller_name}\",${seller_nosalesman},\
			${seller_phone},${seller_pro})"
	echo $request | mysql --defaults-extra-file=mysql.cnf
fi

# Media
media=`./get.sh images $1`
nbi=`echo $media | wc -w`
img=`echo $media | sed 's/ /\", /g' | sed 's/http/\"http/g'`
# Check if adds doesn't contains any images
	if [[ ${media:0:5} != "https" ]]; then
		media=""
		nbi=0
	fi
nul=`for (( i = ${nbi}; i < 10; i++ )); do echo -n ,\"\"; done`
if [[ nbi -eq 0 ]]; then
	request="insert into media values(null, ${nbi} ${nul})"
else
	request="insert into media values(null, ${nbi}, ${img}\"${nul})"
fi
request=${request}"; select last_insert_id() as '';"
media_id=`echo ${request} | mysql --defaults-extra-file=mysql.cnf`

# Gearbox
gearbox=`./get.sh gearbox_name $1`
request="select count(1) as '' from gearbox \
where gearbox_name like \"${gearbox}\""
test=`echo $request | mysql --defaults-extra-file=mysql.cnf`
if [[ ${test} -eq 0 ]]; then
	request="insert into gearbox values(null,\"${gearbox}\")"
	echo $request | mysql --defaults-extra-file=mysql.cnf
fi
request="select gearbox_id as '' from gearbox \
where gearbox_name like \"${gearbox}\""
gearbox_id=`echo $request | mysql --defaults-extra-file=mysql.cnf`

# Add Meta
add_url=`./get.sh url $1`
add_ld=`./get.sh last_publication_date $1`
add_fd=`./get.sh first_publication_date $1`
add_urgent=`./get.sh urgent $1`
add_zipcode=`./get.sh zipcode $1`
request="insert into addmeta values(null, 
/* Add url */ \"${add_url}\",
/* Add last publication date */ \"${add_ld}\",
/* Add first publication date */ \"${add_fd}\",
/* Seller id */ ${seller_id},
/* Is add urgent ? */ ${add_urgent},
/* Zip code of add */ ${add_zipcode})"
request=${request}"; select last_insert_id() as '';"
meta_id=`echo "${request}" | mysql --defaults-extra-file=mysql.cnf`

# Add Data
add_mileage=`./get.sh mileage $1`
add_price=`./get.sh price $1`
if [[ $add_price = "Inconnu" ]]; then
	add_price=0
fi
add_negotiation=${data_unknown}
add_car_condition=${data_unknown}
add_exchange=${data_unknown}
car_date=`./get.sh year $1`


if [[ $add_mileage = "Inconnu" ]]; then
	add_mileage=0
fi

if [[ $car_date = "Inconnu" ]]; then
	car_date=0
fi

request="insert into adddata values(null, \
		${media_id}, \
		${add_mileage}, \
	    ${add_price}, \
		${add_negotiation}, \
		${add_car_condition}, \
		${add_exchange}, \
		${car_date})"
request=${request}"; select last_insert_id() as '';"
data_id=`echo "${request}" | mysql --defaults-extra-file=mysql.cnf`

# Car options
request=`./option_insert_query_generator.sh $1`
request=${request}"; select last_insert_id() as '';"
car_options=`echo ${request} | mysql --defaults-extra-file=mysql.cnf`
 	
# Add add itself
model=`./estimate_model.sh $1`

# Get sale_status_id of adds
sale_status=`head $1 -c1`
if [[ $sale_status = "0" ]]; then
	sale_status=-1
else
	sale_status=1
fi

# Get add id

# request="insert into adds values( \
# /* Add id */ ${add_zipcode}, \
# /* Addmetadata id */ ${meta_id}, 
# /* Adddata id */ ${data_id}, \
# /* Model id */ ${model}, \
# /* Is sell active or dead ? */ ${sale_status}, \
# /* Title of add : */ \"`./get.sh title $1`\", \
# /* Description of add : */ \"`./get.sh description $1`\", \
# /* Car options id */ ${car_options}, \
# /* Gearbox id */ ${gearbox_id},
# /* Fuel id */ ${fuel_id})"

description=`./get.sh description $1 | sed -e "s/[\"';&]/ /g"`
title=`./get.sh title $1 | sed -e "s/[\";&]/ /g"`
request="insert into adds values(${add_id},${meta_id},${data_id},${model},${sale_status},\"${title}\",\"${description}\",${car_options},${gearbox_id},${fuel_id})"
request=${request}"; select last_insert_id() as '';"
add_id=`echo "${request}" | mysql --defaults-extra-file=mysql.cnf`

# echo "${request}"


exit 0
