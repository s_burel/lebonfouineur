#!/bin/bash
request="select car_type_id, car_type_name from model natural join car_type natural join modelstats where model_price_average_cleaned is not null group by car_type_id"

list=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2`

echo "<?php include 'security.php'; ?>"
while [[ -n $list ]]; do
	buffer=`head -n 1 <<< "$list"`
	list=`tail -n+2 <<< "$list"`
	echo "<a href=\"adds.php?car_type=`cut -f1 <<< "$buffer"`\" class=\"m-3 btn btn-outline-secondary\">`cut -f2 <<< "$buffer"`</a>"
done
