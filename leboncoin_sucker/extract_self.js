var data;
if (url().includes("leboncoin"))
data = document.documentElement.firstElementChild.nextElementSibling.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML;

console.log("Running extract_self.js");

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

/* Url */

function url() {
	return window.location.href
}

function leboncoin_url() {
	return url();
}

/* Add id */

function lacentrale_id() {
	return url().replace(/\D/g,"")+"1";
}

function leboncoin_id() {
	var splittedUrl = url().split('/');
	var id = splittedUrl[splittedUrl.length - 2];
	var id = id.replace(/[^\d]/g,"");
	var id = id*10;
	return id;
}

/* Search algo */

function search_content_approx(tag, text) {
	var aTags = document.getElementsByTagName(tag);
	var searchText = text;

	for (var i = 0; i < aTags.length; i++) {
	  if (aTags[i].innerText.includes(searchText)) {
		return aTags[i];
	  }
	}
}


function search_content(tag, text) {
	var aTags = document.getElementsByTagName(tag);
	var searchText = text;

	for (var i = 0; i < aTags.length; i++) {
	  if (aTags[i].textContent == searchText) {
		return aTags[i];
	  }
	}
}

function leboncoin_get_element_from_key(data, key, jump, end_char) {
	var result = "";
	var start_index = data.search(key);
	if (key[0] == "\"") start_index++;
	while(data[start_index] != "\"")start_index++;
	start_index += jump;
	while(data[start_index] != end_char) {
		result = result.concat(data[start_index]);
		start_index++;
	}
	return result;
}

/* Brand */

function lacentrale_brand() {
	return document.getElementById('recherche-react-detailBreadcruumb').firstChild.firstChild.firstChild.childNodes[2].firstChild.firstChild.textContent.replace(" occasion","");	
}

function leboncoin_brand() {
    var elements = document.querySelectorAll('[data-qa-id="criteria_item_brand"]');
    if (elements.length == 0) return "Inconnu";
    return elements[0].textContent.replace("Marque","");
}

/* Year of car */

function lacentrale_year() {
	return search_content("span","Année :").nextElementSibling.textContent.replace("\n","").replace("\n","");
}

function leboncoin_year() {
    var elements = document.querySelectorAll('[data-qa-id="criteria_item_regdate"]');
    if (elements.length == 0) return "Inconnu";
    return elements[0].textContent.replace(/[^\d]/g,"");
}

/* Mileage */

function lacentrale_mileage() {
	return search_content("span","Kilométrage :").nextElementSibling.textContent.replace("\n","").replace("\nKm\n","");
}

function leboncoin_mileage() {
    var elements = document.querySelectorAll('[data-qa-id="criteria_item_mileage"]');
    if (elements.length == 0) return "Inconnu";
    return elements[0].textContent.replace(/[^\d]/g,"");
}

/* Gearbox */

function lacentrale_gearbox_name() {
	var gbn = search_content("span","Boîte de vitesse :").nextElementSibling.textContent.replace("\n","").replace("\n","");
	if (gbn.includes("mécanique"))
		return "Manuelle";
	return "Automatique";
}

function leboncoin_gearbox_name() {
    var elements = document.querySelectorAll('[data-qa-id="criteria_item_gearbox"]');
    if (elements.length == 0) return "Inconnu";
    return elements[0].textContent.replace("Boîte de vitesse","");
}

function lacentrale_gearbox_id() {
	var lgn = lacentrale_gearbox_name();
	if (lgn.includes("écanique"))
		return "1";
	if (lgn.includes("uto"))
		return "2";
	return "-1";
}

function leboncoin_gearbox_id() {
	return parseInt(leboncoin_get_element_from_key(data,"gearbox", 11 , "\""));
}

/* Publication date */

function lacentrale_published_for_n_days() {
	return search_content_approx("span","publié").innerText.replace(/\D/g,"");
}

function lacentrale_published_for_n_ms() {
	return lacentrale_published_for_n_days() * 24 * 60 * 60 * 1000;
}

function lacentrale_last_publication_date() {
	var lpd = new Date(Date.now() - (lacentrale_published_for_n_ms()));
	return (lpd.getYear()+1900) + "-" +
	       (lpd.getMonth()+1) + "-" +
	       (lpd.getDay()+2) + " " +
	       (lpd.getHours()) + ":" +
	       (lpd.getMinutes()) + ":" +
	       lpd.getSeconds();
}

function lacentrale_first_publication_date() {
	return lacentrale_last_publication_date();
}

function leboncoin_first_publication_date() {
	return leboncoin_get_element_from_key(data,"first_publication_date", 3, "\"")
}

function leboncoin_last_publication_date() {
	return leboncoin_get_element_from_key(data,"index_date", 3 , "\"")
}

/* Description */

function leboncoin_description() {
    var elements = document.querySelectorAll('[data-qa-id="adview_description_container"]');
    if (elements.length == 0) return "Inconnu";
    var desc = elements[0].firstChild.firstChild.innerHTML;
    // desc = desc.replace(/<br\s*\/?>/mg,"\n");
    return desc;
}

function lacentrale_description() {
    return lacentrale_title();
}


/* Model */

function lacentrale_model() {
    return document.getElementById('recherche-react-detailBreadcruumb').firstChild.firstChild.firstChild.childNodes[3].firstChild.firstChild.textContent.replace("","");
}

function leboncoin_model() {
    var elements = document.querySelectorAll('[data-qa-id="criteria_item_model"]');
    if (elements.length == 0) 
    	return leboncoin_brand();
    return elements[0].textContent.replace("Modèle","");
}

/* Fuel */

function lacentrale_fuel_name() {
	return search_content("span","Énergie :").nextElementSibling.textContent.replace("\n","").replace("\n","");
}

function leboncoin_fuel_name() {
    var elements = document.querySelectorAll('[data-qa-id="criteria_item_fuel"]');
    if (elements.length == 0) return "Inconnu";
    return elements[0].textContent.replace("Carburant","");
}

function lacentrale_fuel_id() {
	var lfn = lacentrale_fuel_name();
	if (lfn.includes("ssence"))
		return "1";
	if (lfn.includes("iesel"))
		return "2";
	return "-1";
}

function leboncoin_fuel_id() {
	return parseInt(leboncoin_get_element_from_key(data,"fuel", 11 , "\""));
}

/* Image */

function lacentrale_images() {
	return "";
}

function leboncoin_images() {
	var result = "";
	var start_index = data.search("urls");
	start_index += 8;
	while(data[start_index] != "]") {
		result = result.concat(data[start_index]);
		start_index++;
	}
	result = result.replace(/\",\"/g, " ");
	result = result.replace(/\\u002F/g, "/");
	result = result.slice(0, -1);
	return result;
}

/* Price */

function lacentrale_price() {
	return document.getElementsByClassName("cbm-price")[0].firstElementChild.firstElementChild.textContent.replace(/\D/g,"");
}

function leboncoin_price() {
    var elements = document.querySelectorAll('[data-qa-id="adview_price"]');
    if (elements.length == 0) return "Inconnu";
    if (elements.length == 0) 
    	return leboncoin_brand();
    return elements[0].textContent.replace(/[^0-9]/g,"");
}

/* Seller */

function lacentrale_seller_id() {
	return "-1";
}

function leboncoin_seller_id() {
	return leboncoin_get_element_from_key(data,"store_id", 3 , "\"")
}

function leboncoin_seller_phone() {
	return "0000000000";
}

function lacentrale_seller_phone() {
	return "0000000000";
}

function lacentrale_seller_name() {
	return "No name : la centrale";
}

function leboncoin_seller_name() {
	return leboncoin_get_element_from_key(data,"\"name\"", 3 , "\"")
}

function leboncoin_seller_no_salesman() {
	return leboncoin_get_element_from_key(data,"no_salesmen", 2 , ",").replace("}","");
}

function lacentrale_seller_no_salesman() {
	return "true";
}

function lacentrale_seller_status() {
	return "pro";
}

function leboncoin_seller_status() {
	return leboncoin_get_element_from_key(data,"\"type\"", 3, "\"");
}

/* Urgent */

function lacentrale_urgent() {
	return "false";
}

function leboncoin_urgent() {
	return leboncoin_get_element_from_key(data,"\"urgent\"", 2 , ",").replace("}","");
}

/* Title */

function lacentrale_title() {
	return search_content("span","Version :").nextElementSibling.textContent.replace("\n","").replace("\n","");

}

function leboncoin_title() {
    var elements = document.querySelectorAll('[data-qa-id="adview_title"]');
    if (elements.length == 0) return "Inconnu";
    return elements[1].textContent;
}

/* Zipcode */

function lacentrale_zipcode() {
	return document.getElementsByClassName("cbm-sellerName")[1].textContent.replace(/.*\(dpt/g,"").replace(/\D/g,"")+"000";
}

function leboncoin_zipcode() {
	return leboncoin_get_element_from_key(data,"\"zipcode\"", 3, "\"");
}

/* Status */

function lacentrale_status() {
	return "active";
}


function leboncoin_status() {
	if (document.title == "Annonce introuvable")
		return "dead";
	return "active";
}

function lacentrale_host() {
	return "La Centrale";
}

function host() {
	return "Le Bon Coin";
}

function run() {
	console.log("POST");
	if (url().includes("leboncoin")) {	
		if (leboncoin_status() == "dead")
			post('http://localhost/save.php/', {
				id : leboncoin_id(),
				status : leboncoin_status()});
		else
		post('http://localhost/save.php/', {
			id : leboncoin_id(),
			status : leboncoin_status(),
			host : host(),
			url : leboncoin_url(),
			brand : leboncoin_brand(),
			year : leboncoin_year(),
			mileage : leboncoin_mileage(),
			gearbox_name : leboncoin_gearbox_name(),
			description : leboncoin_description(),
			model : leboncoin_model(),
			first_publication_date : leboncoin_first_publication_date(),
			last_publication_date : leboncoin_last_publication_date(),
			fuel_name : leboncoin_fuel_name(),
			fuel_id : leboncoin_fuel_id(),
			gearbox_id : leboncoin_gearbox_id(),
			images : leboncoin_images(),
			price : leboncoin_price(),
			seller_id : leboncoin_seller_id(),
			seller_phone : leboncoin_seller_phone(),
			seller_name : leboncoin_seller_name(),
			no_salesmen : leboncoin_seller_no_salesman(),
			seller_status : leboncoin_seller_status(),
			urgent : leboncoin_urgent(),
			title : leboncoin_title(),
			zipcode : leboncoin_zipcode()
		});
	} /* If Lacentrale is host */
	else 
		post('http://localhost/save.php/', {
        id : lacentrale_id(),
        status : lacentrale_status(),
        host : lacentrale_host(),
        url : url(),
        brand : lacentrale_brand(),
        year : lacentrale_year(),
        mileage : lacentrale_mileage(),
        gearbox_name : lacentrale_gearbox_name(),
        description : lacentrale_description(),
        model : lacentrale_model(),
        first_publication_date : lacentrale_first_publication_date(),
        last_publication_date : lacentrale_last_publication_date(),
        fuel_name : lacentrale_fuel_name(),
        fuel_id : lacentrale_fuel_id(),
        gearbox_id : lacentrale_gearbox_id(),
        images : lacentrale_images(),
        price : lacentrale_price(),
        seller_id : lacentrale_seller_id(),
        seller_phone : lacentrale_seller_phone(),
        seller_name : lacentrale_seller_name(),
        no_salesmen : lacentrale_seller_no_salesman(),
        seller_status : lacentrale_seller_status(),
        urgent : lacentrale_urgent(),
        title : lacentrale_title(),
        zipcode : lacentrale_zipcode()
		});

}

setTimeout(function() {
	window.close();
}, 30000);


window.addEventListener ("load", run, false);
