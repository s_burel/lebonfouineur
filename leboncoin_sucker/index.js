
console.log("Running index.js");


/**
 * Is host leboncoin ?
 */
function url() {
	return window.location.href
}

function host_is_leboncoin() {
 return (window.location.href.includes("leboncoin"));
}

/* Get adds array */

function lacentrale_get_adds_array() {
	var elements = document.getElementsByClassName("resultListContainer")[0].getElementsByClassName("linkAd");
	result = [];

	for (var i = elements.length - 1; i >= 0; i--) {
		var buffer = elements[i].href;
		result.push(buffer);
	}
	return result;
}

function leboncoin_get_adds_array() {
	var elements = document.querySelectorAll('[data-qa-id="aditem_container"]');
	result = [];

	for (var i = elements.length - 1; i >= 0; i--) {
		var buffer = elements[i].firstElementChild.nextElementSibling.href;
		if (typeof buffer === "undefined") continue;
		else result.push(buffer);
	}
	return result;
}

/* Next page */

function lacentrale_next_page() {
	var url = window.location.href;
	var splittedUrl = url.split('page=');
	return splittedUrl[0] + "page=" + (parseInt(splittedUrl[1]) + 1);
}

function leboncoin_next_page() {
	var url = window.location.href;
	var splittedUrl = url.split('/');
	if (splittedUrl.length == 6)
		return url + "p-2";
	url = "";
	for (var i = 0; i < 5; i++) {
		url += splittedUrl[i];
		url += "/";
	}
	var page = splittedUrl[5];
	page = parseInt(page.replace(/[^\d]/g, ""));
	// Loop if fat url
	if (page > 10000)
		return url;
	return url + "p-" + (page+1);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function run_x_url(array, index, length) {
	if (index == length) {
		setTimeout(function() {
			if (host_is_leboncoin()) window.open(leboncoin_next_page(), '_self');
			else window.open(lacentrale_next_page(), '_self');
		}, 100);
		return true;
	}
	setTimeout(function() {
		run_x_url(array, index+1, length);
	}, 100);
	chrome.runtime.sendMessage({add: array[index]});
}

function run() {
	var array;
	if (host_is_leboncoin()) array = leboncoin_get_adds_array();
	else array = lacentrale_get_adds_array();
	run_x_url(array, 0, array.length);
}

window.addEventListener ("load", run, false);
