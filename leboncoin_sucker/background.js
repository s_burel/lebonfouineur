var listOfUrl = [];
var numberOfTabs = 1;

function updateCount(tabId, is_new) {
    if (is_new) numberOfTabs++;
    else numberOfTabs--;
    console.log("Number of Tabs : " + numberOfTabs);
}

chrome.tabs.onRemoved.addListener(
  (tabId) => { updateCount(tabId, false);
});
chrome.tabs.onCreated.addListener(
  (tabId) => { updateCount(tabId, true);
});

function go_if_not_in_history(url) {
    chrome.history.getVisits({ "url": url }, function(visitItems) {
        /* If it has not been visited, add it to <unvisitedURLs> */
        if (!visitItems || (visitItems.length == 0)) {
            listOfUrl.push(url)
        }
    });
}

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    go_if_not_in_history(request.add);
});

function main() {
    if (listOfUrl.length > 0 && numberOfTabs < 6)
        window.open(listOfUrl.shift(), '_blank');
    setTimeout(function() {
        main();
    }, 100);
}

console.log("Running background.js");
main();