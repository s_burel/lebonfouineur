# Database reset
drop database if exists lbf;

# Database create
create database lbf;
use lbf;

# Create seller connection
create table connection(
	request_connection_id int unsigned not null auto_increment primary key,
	request_ip            varchar(64),
	request_brand         varchar(64),
	request_title         varchar(64),
	request_description   text,
	request_date          int,
	request_fuel          varchar(64)
	);

# Create seller table
create table seller( 
	seller_id          int          primary key not null,
	seller_name        varchar(64),
	seller_nosalesman  bool         default true,
	seller_phone       int,
	seller_pro         bool         default false
	);

# Create media table
create table media( 
	media_id int unsigned not null auto_increment primary key,
	nbi      int,
	img0     varchar(100),
	img1     varchar(100),
	img2     varchar(100),
	img3     varchar(100),
	img4     varchar(100),
	img5     varchar(100),
	img6     varchar(100),
	img7     varchar(100),
	img8     varchar(100),
	img9     varchar(100)
	);

# Create fuel table
create table fuel(
	fuel_id   int unsigned primary key,
	fuel_name varchar(16)
	);
insert into fuel value(1, "Essence");
insert into fuel value(2, "Diesel");

# Create gearbox table
create table gearbox(
	gearbox_id   int unsigned not null auto_increment primary key,
	gearbox_name varchar(16)
	);
insert into gearbox value(1, "Manuelle");
insert into gearbox value(2, "Automatique");

create table car_type(
	car_type_id   int unsigned not null auto_increment primary key,
	car_type_name varchar(16)
	);
insert into car_type value(1, "Inconnu");


# Create brand table
create table brand(
	brand_id   int unsigned not null auto_increment primary key,
	brand_name varchar(16)
	);
insert into brand value(1, "Inconnu");

# Create add metadata
create table addmeta(
	addmeta_id     int unsigned not null auto_increment primary key,
	add_url        varchar(128),
	add_last_date  datetime,
	add_first_date datetime,
	seller_id      int,
	add_urgent     bool,
	add_zipcode    int
	);

# Create status
create table status(
	status_id   int unsigned auto_increment primary key,
	status_name varchar(32)
	);
insert into status value(null, "Inconnu");
insert into status value(null, "Négotiation possible");
insert into status value(null, "Négotiation impossible");
insert into status value(null, "Echange possible");
insert into status value(null, "Echange impossible");
insert into status value(null, "Epave");
insert into status value(null, "Travaux nécessaires");
insert into status value(null, "Bon état");
insert into status value(null, "Parfait état");

# Create adds sale status
create table salestatus(
	sale_status_id   int auto_increment primary key,
	sale_status_name varchar(32)
	);
insert into salestatus value(1, "Annonce active");
insert into salestatus value(-1, "Annonce fermée");

# Create add data
create table adddata(
	adddata_id        int unsigned auto_increment primary key,
	media_id          int,
	add_mileage       int,
	add_price         int,
	add_negotiation   int,
	add_car_condition int,
	add_exchange      int,
	add_car_date      int
	);

# Create add table
create table adds(
	add_id          bigint unsigned primary key,
	addmeta_id      int,
	adddata_id      int,
	model_id        int,
	sale_status_id  int,
	add_title       varchar(64),
	add_description text,
	car_options_id  int,
	gearbox_id      int,
	fuel_id         int
	);

create table model(
	model_id         int unsigned auto_increment primary key,
	model_name       varchar(64),
	brand_id         int,
	car_type_id      int,
	model_keyword    varchar(255),
	model_first_year int,
	model_last_year  int
	);
insert into model value(0, "Inconnu", 1, 1, "Inconnu", 0, 0);

create table modelstats(
    primary key (model_id, model_year),
	model_id                      int,
	model_year                    int,
	model_number_of_adds          int,
	model_price_d10               int,
	model_price_d90               int,
	model_price_median            int,
	model_price_average           int,
	model_price_average_cleaned   int,
	model_mileage_d10             int,
	model_mileage_d90             int,
	model_mileage_median          int,
	model_mileage_average         int,
	model_mileage_average_cleaned int
);

create table car_options(
	car_options_id       int unsigned auto_increment primary key,
	demarrage_en_cote    bool default false,
	antidemarrage        bool default false,
    barre_de_toit        bool default false,
    lave_glace_chauffant bool default false,
    capteur_pluie        bool default false,
    vitre_electrique     bool default false,
    climatisation        bool default false,
    demarrage_sans_cle   bool default false,
    jantes               bool default false,
    antibrouillard       bool default false,
    phares_xenon         bool default false,
    interieur_cuir       bool default false,
    radar_recul          bool default false,
    regulateur_vitesse   bool default false,
    limitateur_vitesse   bool default false,
    radio_mp3            bool default false,
    bluetooth            bool default false,
    gps                  bool default false
	);
