<?php
$main = true;
?>
<?php include 'security.php'; ?>
<?php include 'profiling.php'; ?>


<?php profiling_start(); ?>
<!doctype html>
<html lang="en">
<?php include 'head.php'; ?>


<?php profiling_next("Head : "); ?>
  <body>
<?php include 'header.php';?>
<div class="row">
  <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
    <form>
      <div class="form-row">
        <div class="col-auto">
            <?php include 'departements.php';?>
        </div>
        <div class="col-auto">
            <?php include 'brands_form.php';?>
        </div>
        <div class="col-auto">
            <?php include 'models_form.php';?>
        </div>
        <div class="col-auto">
            <?php include 'car_type_form.php';?>
        </div>
        <div class="col-auto">
            <?php include 'fuel_form.php';?>
        </div>
        <div class="col-auto">
            <?php include 'form_price.php';?>
        </div>
        <div class="col-auto">
          <button type="submit" class="btn btn-primary mb-2">Rechercher</button>
        </div>
      </div>



    </form>
  </div>
</div>

<?php profiling_next("Form : "); ?>

<?php 


$request = "select *, (round(100/(model_price_average_cleaned/add_price),0)) as 'pct' from adds natural join model natural join adddata natural join brand natural join addmeta natural join modelstats where datediff(now(),add_last_date) < 14 and model_year = add_car_date and model_price_average_cleaned is not NULL";

if (isset($_GET['dept'])     and $_GET['dept']     != 0) $request = $request." and floor(add_zipcode/1000) = ".$_GET['dept'];
if (isset($_GET['model'])    and $_GET['model']    != 0) $request = $request." and model_id = ".$_GET['model'];
if (isset($_GET['brand'])    and $_GET['brand']    != 0)$request =  $request." and brand_id = ".$_GET['brand'];
if (isset($_GET['car_type']) and $_GET['car_type'] != 0)$request =  $request." and car_type_id = ".$_GET['car_type'];
if (isset($_GET['fuel'])     and $_GET['fuel']     != 0)$request =  $request." and fuel_id = ".$_GET['fuel'];
if (isset($_GET['price'])     and $_GET['price']     != 0) {
  $price_low = ((intval($_GET['price']) * 8) / 10 - 400);
  $price_hgh = ((intval($_GET['price']) * 12) / 10);
  $request =  $request." and add_price >= ".$price_low." and add_price <= ".$price_hgh;
}
$request = $request." order by add_last_date desc limit ";
if (isset($_GET['page'])) $request = $request."30 offset ".((($_GET['page']-1)*30)+1);
else $request = $request."30 offset 0";

?>

<?php profiling_next("Request build : "); ?>


<?php

include 'auth.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

profiling_next("SQL Connect : ");
/* Delete link generation */
if (strpos($_SERVER['REQUEST_URI'], "?") == false) {
  $link=$_SERVER['REQUEST_URI']."?obs=";
} else {
  $i = strpos($_SERVER['REQUEST_URI'], "obs=");
  if ($i == false) {
    $link=$_SERVER['REQUEST_URI']."&obs=";
  } else {
   $link = $_SERVER['REQUEST_URI'];
   $link = substr($link, 0, $i - 1);
   if (strpos($link, "?") == false) $link = $link."?obs=";
   else $link = $link."&obs=";
  }
}
profiling_next("Delete link generation : ");



/* If obsolete order, then destroy add : */
if (isset($_GET['obs'])) {
  $destroy_request = "update adds set sale_status_id = sale_status_id * -1 where add_id = ".$_GET['obs'].";";
  $conn->query($destroy_request);
  profiling_next("Destroy add : ");

}


/* Data query */
$conn->query("SET NAMES 'utf8'");
$result = $conn->query($request);

profiling_next("SQL Main Query : ");


// $count_request = "select count(1)/30 as nbpage from (".$request.") as t;";
$count_request = "select count(1)/30 as nbpage from (".preg_replace ( "/limit.*/" , "" , $request).") as t;";
profiling_next("SQL Page number query generation ");

$result_count = $conn->query($count_request);
$row_count = $result_count->fetch_assoc();
$page_count = $row_count["nbpage"];

profiling_next("SQL Mysql Page number query : ");


?>

<div id="pagination_up" class="row col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
<?php include 'pagination.php';?>
</div>
<?php profiling_next("Pagination generation : "); ?>
<div class="row col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">


<?php

if ($result->num_rows > 0) {
echo "<table class='text-center table table-sm table-striped'>
<thead>
    <tr>
      <th scope='col'>Marque</th>
      <th scope='col'>Modèle</th>
      <th scope='col'>Année</th>
      <th scope='col'>Adresse</th>
      <th scope='col'>Cote</th>
      <th scope='col'>Prix</th>
      <th scope='col'>Rapport</th>
      <th scope='col'>Info</th>
      <th scope='col'>Lien</th>
    </tr>
  </thead>
    <tbody>";
    while($row = $result->fetch_assoc()) {
echo"<tr><th scope='row'>".
$row["brand_name"]."</td><td>".
$row["model_name"]."</th><td>".
$row["add_car_date"]."</th><td>".
$row["add_zipcode"]."</td><td>".
$row["model_price_average_cleaned"]."€</td><td class='font-weight-bold'>".
$row["add_price"]."€</td>";
if ($row["pct"] > 110) {
  echo "<td class='my-1 text-center text-white bg-danger' ><h5>+".($row["pct"]-100)."%</h5></td>";
} else if ($row["pct"] > 100) {
  echo "<td class='my-1 text-center text-white bg-warning'><h5>+".($row["pct"]-100)."%</h5></td>";
} else if ($row["pct"] == 100) {
  echo "<td class='my-1 text-center text-white bg-warning'><h5> = </h5></td>";
} else if ($row["pct"] > 90) {
  echo "<td class='my-1 text-center text-white bg-warning'><h5>-".(100-$row["pct"])."%</h5></td>";
} else {
  echo "<td class='my-1 text-center text-white bg-success' ><h5>-".(100-$row["pct"])."%</h5></td>";
}
echo "<td><i class='glyphicon glyphicon-cloud'></i>";

$time = floor((time()-strtotime($row["add_last_date"]))/86400);
echo '
<a style="color:black;" href="#" data-toggle="tooltip" title="Annonce passée il y a '.$time.' jours"><i class="far fa-clock"></i></a>
';

if ($row["add_mileage"] <= $row["model_mileage_d10"]) {
echo '
<a style="color:red;" href="#" data-toggle="tooltip" title="Le kilométrage est anormalement bas pour un véhicule de ce modèle et de cette année. Il peut être trafiqué"><i class="fas fa-tachometer-alt"></i></a>
';
}

if ($row["sale_status_id"] == -1) {
echo '
<a style="color:red;" href="#" data-toggle="tooltip" title="L\'annonce a été signalée vendue ou supprimée"><i class="fas fa-ban"></i></a>
';
}


// echo floor((time()-strtotime($row["add_last_date"]))/86400);
echo "</td>";
echo "<td><a href='".$row["add_url"]."' type='button' class='btn btn-outline-secondary btn-sm mr-3' target='_blank'>Aller</a>";
if ($row["sale_status_id"] == 1)
  echo '<a style="color:black;" href="'.$link.$row["add_id"].'" data-toggle="tooltip" title="Déclarer l\'annonce comme vendue / supprimée"><i class="far fa-trash-alt"></i></a>';
else   echo '<a style="color:black;" href="'.$link.$row["add_id"].'" data-toggle="tooltip" title="Déclarer l\'annonce comme valide"><i class="fas fa-check"></i></a>';
echo "</td></tr>";

}
echo "</tbody></table>";
} else {
    echo "
    <div class='row py-3 rounded bg-white border border-primary w-100'>
      <h1 class=' mx-auto d-block'>Aucun résultat trouvé selon vos critères de recherche</h1>
    </div>";
}

profiling_next("Display adds : ");

$conn->close();
?>

  </div>
</div>

<div id="pagination_down" class="row col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
</div>


    <script language="javascript">
      document.getElementById('pagination_down').innerHTML = document.getElementById('pagination_up').innerHTML;
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script type="text/javascript">
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
<?php profiling_next("Script : "); ?>
  </body>
</html>