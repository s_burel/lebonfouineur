<?php
$main = true;
?>
<?php include 'security.php'; ?>
<!doctype html>
<html lang="en">
 <?php include 'head.php'; ?>

 <body>

<?php include 'header.php';?>

<div class="row">
  <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
    <div class="my-2 alert alert-primary text-center" role="alert">
    <h1>Annonces du jour</h1>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-12 col-sm-1 col-md-2"></div>
  <div class="col-12 col-sm-5 col-md-4">
    <?php include 'franceDepartmentsLow.svg'; ?>
  </div>
  <div class="col-12 col-sm-5 col-md-4">
    <div class="p-3 text-center">
        <h3 class="text-center">Marque :</h3>
    </div>
    <div>
      <?php include 'brands_buttons.php'; ?>
    </div>
    <div class="p-3 text-center">
    <h3 class="text-center">Classe :</h3>
    </div>
    <div>
      <?php include 'car_type_buttons.php'; ?>
    </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>