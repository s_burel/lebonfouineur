<?php
/* All others files must include <?php include 'security.php'; ?> */
if (!isset($security_check)) {

  if (!isset($main)) {
    header('Location: http://www.tipiak.fr');
    exit();
  }

  if (isset($_GET['add']))
    if (preg_match("/[^0-9]/", $_GET['add'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['brand']))
    if (preg_match("/[^0-9]/", $_GET['brand'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['car_type']))
    if (preg_match("/[^0-9]/", $_GET['car_type'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['dept']))
    if (preg_match("/[^0-9]/", $_GET['dept'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['fuel']))
    if (preg_match("/[^0-9]/", $_GET['fuel'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['id']))
    if (preg_match("/[^0-9]/", $_GET['id'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['model']))
    if (preg_match("/[^0-9]/", $_GET['model'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['obs'])) /* Obsolete add */
    if (preg_match("/[^0-9]/", $_GET['obs'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['page']))
    if (preg_match("/[^0-9]/", $_GET['page'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }
  if (isset($_GET['price']))
    if (preg_match("/[^0-9]/", $_GET['price'])) {
      header('Location: http://www.tipiak.fr');
      exit();
    }

  $security_check = true;
}

?>
