<?php include 'security.php'; ?>
<?php

$profiling_start = 0;
$profiling_stop  = 0;

function profiling_start()
{
	global $profiling_start;
	$profiling_start = profiling_what_time_is_it();
}

function profiling_stop($text)
{
	global $profiling_start;
	$profiling_stop = profiling_what_time_is_it();
	$profiling_stop = $profiling_stop - $profiling_start;
	echo $text ." time : ".number_format($profiling_stop, 6)."<br/>";
}

function profiling_next($text)
{
	profiling_stop($text);
	profiling_start();
}

function profiling_what_time_is_it()
{
    list($usec, $sec) = explode(" ", microtime());
	return floatval((float)$usec + (float)$sec);
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return floatval((float)$usec + (float)$sec) - $_SERVER["REQUEST_TIME_FLOAT"];
}
?>
