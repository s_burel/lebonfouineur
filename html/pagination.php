<?php include 'security.php'; ?>
<?php

$url="/adds.php?";

if (isset($_GET['dept']) ) $url = $url."&dept=" .$_GET['dept'];
if (isset($_GET['model'])) $url = $url."&model=".$_GET['model'];
if (isset($_GET['brand'])) $url = $url."&brand=".$_GET['brand'];
if (isset($_GET['car_type'])) $url = $url."&car_type=".$_GET['car_type'];
if (isset($_GET['fuel'])) $url = $url."&fuel=".$_GET['fuel'];
if (isset($_GET['price'])) $url = $url."&price=".$_GET['price'];
if (!(isset($_GET['page'])) or $_GET['page'] == 0) $page = 1;
else $page = $_GET['page'];

if ($page > 10) {
  $target = $page-9;
} else $target = 1;

$limit = 15;
if($page_count < $limit) $limit = $page_count;

?>

<nav class="py-3" aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li <?php
echo "class='page-item ";

if ($page == 1) echo "disabled";
?>'>
      <a class="page-link" href=
<?php 
	echo "'".$url."&page=".($page-1)."'";
?>
       tabindex="-1">
      	Précédent</a>
    </li>
<?php

for ($i=0; $i < $limit ; $i++) {
echo "<li class='page-item";
if ($target == $page) echo " active";
echo "'><a class='page-link' href='";
echo $url."&page=$target";
echo "'>".$target."</a></li>";
$target++;
}

?>
        <li <?php
echo "class='page-item ";
if ($page >= $page_count) echo "disabled";
?>'>
      <a class="page-link" href=
<?php 
	echo "'".$url."&page=".($page+1)."'";
?>
      >Suivant</a>
    </li>
  </ul>
</nav>
