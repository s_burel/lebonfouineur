<?php include 'security.php'; ?>
<?php
$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
$uri = $uri_parts[0];
?>
<nav class="navbar mb-2 navbar-expand-lg navbar-light bg-white border border-primary">
  <a class="navbar-brand" href="#">Le Bon Fouineur</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li 
<?php echo "class='nav-item ";
if (($uri) == "/index.php") echo "active";
if (($uri) == "/") echo "active";
echo "'";?>
><a class="nav-link" href="/index.php">Accueil</a>
      </li>
      <li 
<?php echo "class='nav-item ";
if (($uri) == "/adds.php") echo "active";
echo "'";?>
><a class="nav-link" href="/adds.php">Liste des annonces</a>
      </li>
    </ul>
  </div>
</nav>