#!/bin/bash

mkdir -p data
mkdir -p data/save

while [[ 1 ]]; do
	echo "Moving data folder"
	mv /var/www/html/data data/current

	echo "Recreating data folder"
	mkdir -p /var/www/html/data

	tgz=`date +%y%m%d%H%m`.tgz
	echo "Creation of the save file ${tgz}"
	tar -czf ${tgz} data/current
	mv ${tgz} data/save

	echo "Updating database"
	rm -r data/adds
	mv data/current data/adds
	./run.sh 40
done
