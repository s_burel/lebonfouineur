#!/bin/bash

# Display in standard outline a sql query to insert options of an add

request="insert into car_options value(null"
description=`./get.sh description $1 | sed 'y/áàâäçéèêëîïìôöóùúüñÂÀÄ\
ÇÉÈÊËÎÏÔÖÙÜÑ/aaaaceeeeiiiooouuunAAACEEEEIIOOUUN/' | tr [:upper:] [:l\
ower:]`

# Demarrage en cote
echo ${description} | grep cote > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Antidemarrage
echo ${description} | grep antidemarrage > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Barre de toit
echo ${description} | grep 'barre de toit' > /dev/null ||
echo ${description} | grep 'barres de toit' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Lave-glace chauffant
echo ${description} | grep 'glace chauffant' > /dev/null ||
echo ${description} | grep 'glaces chauffant' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Capteur de pluie
echo ${description} | grep 'capteur de pluie' > /dev/null ||
echo ${description} | grep 'capteurs de pluie' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Vitre Electrique
echo ${description} | grep 'vitre electrique' > /dev/null ||
echo ${description} | grep 'vitres electrique' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Climatisation
echo ${description} | grep 'clim' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Demarrage sans clé
echo ${description} | grep 'demarrage sans cle' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Jantes
echo ${description} | grep 'jante' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Antibrouillard
echo ${description} | grep 'antibrouillard' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Phares Xénon
echo ${description} | grep 'xenon' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Intérieur cuir
echo ${description} | grep 'interieur cuir' > /dev/null ||
echo ${description} | grep 'sieges en cuir' > /dev/null ||
echo ${description} | grep 'siege en cuir' > /dev/null ||
echo ${description} | grep 'sieges cuir' > /dev/null ||
echo ${description} | grep 'siege cuir' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Radar de recul
echo ${description} | grep 'radar de recul' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Régulateur de vitesse
echo ${description} | grep 'regulateur de vitesse' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Limitateur de vitesse
echo ${description} | grep 'limitateur de vitess' > /dev/null ||
echo ${description} | grep 'limiteur de vitesse' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Radio mp3
echo ${description} | grep 'radio' > /dev/null ||
echo ${description} | grep 'mp3' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# Bluetooth
echo ${description} | grep 'bluetooth' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

# GPS
echo ${description} | grep 'gps' > /dev/null
if [[ $? -eq 0 ]]; then
    request="${request}, true"
else
    request="${request}, false"
fi

request="${request})"
echo ${request}