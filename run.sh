#!/bin/bash
if [[ "$#" -lt 1 ]]; then
  echo "Usage: $0 <number_of_threads> [-r]" >&2
  exit 1
fi
# Argument: number of threads used
echo "Updating database..."
	mkdir -p lbf_database/
	git -C lbf_database pull
gcc get.c -o get.sh
gcc estimate_model.c -Wall -g -I/usr/include/mysql -o estimate_model.sh -lmysqlclient
./build.sh $1 $2
./update_all_model_stats.sh $1
./html.sh -g
echo "Done."
