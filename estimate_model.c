/**
 * 
 * This script display, in standard outline,
 * The estimated model id of a car
 */

#include <my_global.h>
#include <mysql.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Macro to access global memory */
#define key(x) (data + x)
#define BRAND 0
#define MODEL 26
#define DATE  52
#define FUEL  78
#define TITLE 296
#define DESC  562
#define TEXT  868

/* Global Memory */
char data[2048] = {0};
/* String that will be used in MySQL query */
char query[180] = "\
select model_id, model_keyword from model natural join brand where (model_first_year <=      and model_last_year >=      and brand_name like '                 )";

/**
 * That funcrion return the searched key
 * First parameter : Key searched
 * Second parameter : File where to search
 * Third parameter : Result buffer (needs to get 256 bytes allocated memory)
 */

int get(char const * key, char const * file, char * result)
{	
	FILE * target;
	char buffer;
	register int result_offset;
	register int compare_offset;
	register int size;

	result_offset=1;
	target = fopen(file, "r");
	if (target == NULL) return 2;

	compare_offset = 0;
	size = strlen(key);

	/* Search for first parameter */
	while (fread(&buffer, sizeof(char), 1, target)) {
		if (compare_offset == size) {
			if (key[compare_offset] == '\0' &&
				buffer=='=') { /* We found the parameter */
					fread(result, sizeof(char) * 256, 1, target);
					while (result[++result_offset] != '\n') {
						if (unlikely(result_offset > 256)) {
							result[--result_offset] = 0;
							return 0;
						}
					}
					result[result_offset] = 0;
					return 0;
			} else compare_offset=0;
		} if (key[compare_offset] == buffer) {
			compare_offset++;
		} else compare_offset=0;
	}
	return 3;
}

/**
 * Function to standardize stuff
 * It copy data from src to dst,
 * lowerize the data, remove spaces
 * and replace ',' by '.'
 * Returns the end of dst
 */
char * standardize(char * dst, char * src) {
	register char a;
	a = *src;
	while(a != 0) {
		if (a >= 65 && a <= 90) { /* Lowerize */
			a = a + 32;
		} else if (a == ',') /* Remove ','' */
		{
			a = '.';
		} else if (a == ' ') { /* Remove spaces */
			goto next;
		}
		*(dst++) = a;
		next:
		a = *(++src);
	}
	return dst;
}


/**
 * Return number of given keywords in given text
 * Params : text : Where to look for keywords
 *          keywords : String where keywords are
 *                     separated by spaces (' ')
 * Return number of given keywords in text, or
 * -2 if all the keywords weren't found
 */

int calculate_score(char * text, char * keywords) {
	int score = 0;
	char kwt[32];
	int i;
loop:
	if (*keywords == ' ') keywords++;
	if (*keywords == 0) return score;
	i=0;
	/* Copy target word */
	while((*keywords) != ' ' && (*keywords) != 0) {
 		kwt[i++] = *(keywords++);
	}
	kwt[i++] = 0;

	/* Search target word */
	if (likely(strstr(text, kwt) == NULL)) {
		return -2;
	}
	score++;
	goto loop;
}

int main(int argc, char const *argv[])
{
	register int i;
	/* Variable used to offset global text */
	register char * target;
	/* MYSQL variable */
    MYSQL mysql;
	MYSQL_ROW row;
    MYSQL_RES * result;
	/**
	 * Variable used during keywords comparison 
	 * between standardized text and mysql data
	 */
	char best [10] = "1";
	int best_sc = -1;

	if (argc < 2)
		return -1;
	/* Copy key from file passed in parameter */

	get("brand"      , argv[1], key(BRAND));
	get("model"      , argv[1], key(MODEL));
	get("year"       , argv[1], key(DATE)) ;
	get("fuel_name"  , argv[1], key(FUEL)) ;
	get("title"      , argv[1], key(TITLE));
	get("description", argv[1], key(DESC)) ;

	/* Antibug check */
	if ((*key(DATE) == 'I') || (key(DATE) == 0) || (key(BRAND) == 0)) {
		printf("1");
		return 0;
	}
 
	/* Generation of data text */
	target = standardize(key(TEXT), key(TITLE));
	target = standardize(target, key(BRAND));
	target = standardize(target, key(MODEL));
	target = standardize(target, key(FUEL));
	         standardize(target, key(DESC));
	         *target = 0;

	/* Query generator */
	*((int*) (query + 88)) = *((int*) (key(DATE)));
	*((int*) (query + 116)) = *((int*) (key(DATE)));
	i = 0;
	while(*(key(BRAND)+i)) {
		*(query + 142 + i) = *(key(BRAND)+ i);
		i++;
		}
	*(query + 142 + i) = '\'';


	/* Mysql Connect and fetch */
    mysql_init(&mysql);
    if(mysql_real_connect(&mysql,"localhost","lbf","","lbf",0,NULL,0) == 0)
    {
        printf("%s\n",mysql_error(&mysql));
        return 1;
    }
    if (mysql_query(&mysql, query))
	{
		printf("%s\n",mysql_error(&mysql));
		return 1;
	}

    result = mysql_store_result(&mysql);
    if (result == NULL) 
    {
		printf("%s\n",mysql_error(&mysql));
		return 1;
    }
    mysql_close(&mysql);

	/* For each row compare to adds, if all match we give a five */  

	while ((row = mysql_fetch_row(result))) 
	{
		int sc = calculate_score(key(TEXT), row[1]);
		if (sc >= best_sc) /* = is important for getting last inserted model */
		{
			strcpy(best, row[0]);
			best_sc = sc;
		}
	}

  	/* Display result */
	printf("%s\n", best);
	return 0;
}