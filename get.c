#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * This script display, in standard outline,
 * The requested content of given file
 * First parameter : Key searched
 * Second parameter : File where to search
 */

/* Bash equivalent : cat "$2" | grep -e "^$1=" | cut -d'=' -f2 */

int main(int argc, char const *argv[])
{
	if (argc < 3) exit(1);
	
	FILE * target;
	char buffer;
	register int compare_offset;
	register int size;

	target = fopen(argv[2], "r");
	if (target == NULL) exit(2);

	compare_offset = 0;
	size = strlen(argv[1]);

	/* Search for first parameter */
	while (fread(&buffer, sizeof(char), 1, target)) {
		if (compare_offset == size) {
			if (argv[1][compare_offset] == '\0' &&
				buffer=='=') {
					while (fread(&buffer, sizeof(char), 1, target) && buffer != '\n')
						printf("%c", buffer);
					printf("\n");
					exit(0);
			} else compare_offset=0;
		} if (argv[1][compare_offset] == buffer) {
			compare_offset++;
		} else compare_offset=0;
	}

	return 3;
}