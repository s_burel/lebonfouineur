#!/bin/bash

# Update model stats for a given year
# Parameter : id of model to update in sql database
#             year of the model to update

# Calculate number of adds
request="select count(*) as '' from adds natural join adddata where model_id = $1 and add_car_date = $2"
nba=`mysql --defaults-extra-file=mysql.cnf <<< $request`
# echo Number of adds : $nba

# Delete if exist
request="delete from modelstats where model_id = $1 and model_year = $2"
mysql --defaults-extra-file=mysql.cnf <<< $request

# Security if there is not enought adds in model
if [[ $nba -lt 10 ]]; then
request="replace into modelstats (
model_id, model_year, model_number_of_adds)
values($1, $2, $nba)"
mysql --defaults-extra-file=mysql.cnf <<< $request
exit 0
fi

# Calculate average price
request="select avg(add_price) as '' from adds natural join adddata where model_id = $1 and add_car_date = $2;"
avgp=`mysql --defaults-extra-file=mysql.cnf <<< $request`
# echo Average price : $avgp

# Calculate median price
request="select add_price from adds natural join adddata where model_id = $1 and add_car_date = $2 order by add_price;"
let "buffer = nba / 2"
mp=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2 | head -n $buffer | tail -n 1`
# echo Median price : $mp

# Calculate d10 price
let "buffer = nba / 10"
dp10=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2 | head -n $buffer | tail -n 1`
# echo D10 price : $dp10

# Calculate d90 price
let "buffer = nba / 10"
dp90=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2 | tail -n $buffer | head -n 1`
# echo D90 price : $dp90
# Calculate average price cleaned
request="select avg(add_price) as ''
from adds natural join adddata
where model_id = $1
and add_car_date = $2
and add_price < $dp90
and add_price > $dp10;"
avgpc=`mysql --defaults-extra-file=mysql.cnf <<< $request`
# echo Average price cleaned : $avgpc

# Calculate mileage_average
request="select avg(add_mileage) as '' from adds natural join adddata where model_id = $1 and add_car_date = $2;"
avgm=`mysql --defaults-extra-file=mysql.cnf <<< $request`
# echo Average mileage : $avgm

# Calculate median mileage
request="select add_mileage from adds natural join adddata where model_id = $1 and add_car_date = $2 order by add_mileage;"
let "buffer = nba / 2"
mm=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2 | head -n $buffer | tail -n 1`
# echo Median mileage : $mm

# Calculate d10 mileage
let "buffer = nba / 10"
dm10=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2 | head -n $buffer | tail -n 1`
# echo D10 mileage : $dm10

# Calculate d90 mileage
let "buffer = nba / 10"
dm90=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2 | tail -n $buffer | head -n 1`
# echo D90 mileage : $dm90

# Calculate average mileage cleaned
request="select avg(add_mileage) as ''
from adds natural join adddata
where model_id = $1 and add_car_date = $2
and add_price < $dp90
and add_price > $dp10;"
avgmc=`mysql --defaults-extra-file=mysql.cnf <<< $request`
# echo Average mileage cleaned : $avgmc

request="replace into modelstats (
model_id                      ,
model_year                    ,
model_number_of_adds          ,
model_price_d10               ,
model_price_d90               ,
model_price_median            ,
model_price_average           ,
model_price_average_cleaned   ,
model_mileage_d10             ,
model_mileage_d90             ,
model_mileage_median          ,
model_mileage_average         ,
model_mileage_average_cleaned)
values(
$1,
$2,
$nba,
$dp10,
$dp90,
$mp,
$avgp,
$avgpc,
$dm10,
$dm90,
$mm,
$avgm,
$avgmc)
"
mysql --defaults-extra-file=mysql.cnf <<< $request
