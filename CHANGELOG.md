# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Todo
- Ask Direction Recherche Technologie advise
- Index map of france animation on hover
- Add SQL cache
- Add sign in/up
- Add login and premium package
- Add motors on sql

## [0.2.1] - 2019-01-11
### Removed
- Useless javascript file from leboncoin_sucker
- Outdated HTML page

### Added
- Chrome fetcher from lacentrale
- Script to automatize run script and data saving
- Add Head.php file and GA script
- PHP Profiling script
- Title.php for display nice title
- Add icon in adds.php to declare an add outdated
- Security when copying php to make sure all files get security check
- Add Info label in adds table and adds icons for mileage, adds date, and adds status
- Add year display in adds.php

### Changed
- Update sucker extension script to avoid opening too much tabs
- Adds.php now display adds on two last weeks
- Update all models stats script now reset its table
- HTML Header
- Display frequency on script to build database
- Update database building script to be able to add data without reset the database
- Adds link open now in new tab
- Generate html fom model script to display model alphabetically ordered

### Fixed
- Syntax error in php adds file.
- Bug in pagination that resetted entered search options
- Fix bug in php adds forms that resets model form
- Fix bug in generate_car_type buttons
- Fix bug when searching adds from Corse Departments
- List of displayed adds in adds.php now doesn't grow with page index

## [0.2.0] - 2018-12-25
### Added
- Price form in PHP search engine
- New HTML generation in automatized script
- Html generation script for clean run script
- Fuel and car type search in PHP
- Buttons generators used in main page
- New C compiler estimate model script
- New C compiled getter
- .gitignore file
- Car type column in sql database
- Security to launch run script with enough arguments

### Changed
- Website new Interface
- PHP adds page : new display
- PHP adds page : adds pct interest now based on year equivalent models
- PHP Model form generator to look for model with valid price
- Output of the calculate accuracy script for a user-friendly rendering
- Output of the run script in order to embellish it
- Model Database is now part of a stand-alone project

### Removed
- Useless php index file in project root directory
- Useless scripts to update add status
- Useless new_estimate_model script

## [0.1.3] - 2018-12-16
### Added
- A Changelog
- New models in database
- Id key check on PHP Script
- Syntax check to looking for missing key in insert script
- New Way of calculing model stats : Now taking date into account

### Changed
- SLQ Option query generator to use nex syntax
- Update script to use new getter

### Removed
- Script to convert format
- Obsolete getters
- Obsolete download script

### Fixed
- Syntax error on script run.sh to generate PHP pages

## [0.1.2] - 2018-12-13
### Removed
- Useless script call from run script

### Changed
- Insert script to use new getter
- Get seller script to use new getter
- Estimate model to use new getter

### Fixed
- SQL syntax errors on some adds on insert script
- Syntax error on estimate model script

## [0.1.1] - 2018-08-30
### Added
- Chrome extension to download data
- New getter
- Script to convert data from old format to new one
- Script to fetch data from new way
- BMW Models on database
- PHP File to save data sent from chromium extension
- PHP File to estimate model from post request

## [0.1.0] - 2018-08-20
### Added
- Script to generate the database and website creation
- UTF-8 encoding in SQL usage
- Script to reset all adds

## [0.0.4] - 2018-08-15
### Added
- PHP Files
- Shell Script to detect some PHP Files

### Changed
- SQL Usage for security reasons

### Removed
- Remove empty sections from CHANGELOG, they occupy too much space and
create too much noise in the file. People will have to assume that the
missing sections were intentionally left out because they contained no
notable changes.

## [0.0.3] - 2018-08-12
### Added
- Script to detect adds statistics

### Changed
- Algorithm to detect dead adds


## [0.0.2] - 2018-08-11
### Added
- Script to estimate a model
- Script to generate a SQL query to insert car options of an add
- First car models in database

### Changed
- Algorithm of estimate model script


## 0.0.1 - 2018-08-05
### Added
- Getters to get information from an add
- Download Script
- SQL Install Script
- SQL Database Creation Script