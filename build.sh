#!/bin/bash

# Boolean : do we must reset the databse ?
reset=0

# Argument: number of threads used
number_adds=`ls data/adds/ | wc -l`
number_operations_before_output=${1}
number_operations_done=0
echo "Creating a database containing $number_adds adds..."

if [[ $# -eq 2 && $2 = "-r" ]]; then # Reset database
	reset=1
fi

# Used for multithreading
limit_pid=`ps | wc -l`
let "limit_pid=limit_pid + ${1}"
pid=${base_pid}

if [[ $reset -eq 1 ]]; then
	echo Build Database... &&\
	mysql --defaults-extra-file=mysql.cnf < database.sql &&\
	echo Fill database... &&\
	mysql --defaults-extra-file=mysql.cnf < lbf_database/fill_database.sql
fi

for i in data/adds/*
do
	while [[ ${pid} -ge ${limit_pid} ]]; do
		pid=`ps | wc -l`
		sleep 0.05
	done

	# echo Insert $i
	./insert.sh $i &

	pid=`ps | wc -l`

	let "number_operations_before_output = number_operations_before_output - 1"

	if [[ number_operations_before_output -eq 0 ]]; then 
	let "number_operations_done = number_operations_done + ${1}"
	let "number_adds_percentage = ((number_operations_done*100)/number_adds)"
	number_operations_before_output=${1}
	echo -en "\rDone $number_adds_percentage % of work ($number_operations_done / $number_adds)"
	fi

done

echo -en "\r"