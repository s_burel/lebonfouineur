#!/bin/bash

echo "<?php include 'security.php'; ?>"
echo "<select name='fuel' id='fuel_form' class='form-control'>"
echo "<option value='0'>Carburant</option>"

request="select * from fuel"
list=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2`

while [[ -n $list ]]; do
	buffer=`head -n 1 <<< "$list"`
	list=`tail -n+2 <<< "$list"`
	echo "<option value='`cut -f1 <<< "$buffer"`'>`cut -f2 <<< "$buffer"`</option>"
done

echo "</select>"

echo "<?php "
echo "\$buffer=0;"
echo "if (isset(\$_GET['fuel']) and \$_GET['fuel'] != 0)"
echo "	\$buffer = \$_GET['fuel'];"
echo "echo \"<script language='javascript'>document.getElementById('fuel_form').value='\".\$buffer.\"';"
echo "</script>\" ; ?>"
