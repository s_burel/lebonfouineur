#!/bin/bash

# Argument: number of threads used
request="select count(*) as '' from model;"
number_models=`mysql --defaults-extra-file=mysql.cnf <<< $request`
number_operations_before_output=1
number_operations_done=0
echo "Updating a database containing "$number_models" models..."

# Reseting database
# request="truncate table modelstats"
# mysql --defaults-extra-file=mysql.cnf <<< $request

# Used for multithreading
limit_pid=`ps | wc -l`
let "limit_pid=limit_pid + ${1}"
pid=${base_pid}

request="select model_id from model"
list=`mysql --defaults-extra-file=mysql.cnf <<< $request | tail -n+2`

while [[ -n $list ]]; do
	pid=`ps | wc -l`
	while [[ ${pid} -ge ${limit_pid} ]]; do
		pid=`ps | wc -l`
		sleep 0.05
	done

	buffer=`head -n 1 <<< "$list"`
	list=`tail -n+2 <<< "$list"`

	request="select model_first_year as '' from model where model_id = $buffer"
	fy=`mysql --defaults-extra-file=mysql.cnf <<< $request`

	request="select model_last_year as '' from model where model_id = $buffer"
	ly=`mysql --defaults-extra-file=mysql.cnf <<< $request`

	for (( i = $fy; i < $ly; i++ )); do
		./update_model_stats.sh $buffer $i &
	done

	let "number_operations_before_output = number_operations_before_output - 1"
	let "number_operations_done = number_operations_done + 1"

	if [[ number_operations_before_output -eq 0 ]]; then 
	let "number_models_percentage = ((number_operations_done*100)/number_models)"
	number_operations_before_output=1
	echo -en "\rDone $number_models_percentage % of work ($number_operations_done / "$number_models")"
	fi

done

echo -en "\r"